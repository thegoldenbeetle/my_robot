import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.actions import SetEnvironmentVariable
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch_ros.actions import Node
 
 
def generate_launch_description():
 
    # Include the robot_state_publisher launch file, provided by our own package. Force sim time to be enabled
    package_name='my_robot'
 
    rsp = IncludeLaunchDescription(
                PythonLaunchDescriptionSource([os.path.join(
                    get_package_share_directory(package_name),'launch','rsp.launch.py'
                )]), launch_arguments={'use_sim_time': 'true'}.items()
    )
 
    # use world and models
    world = os.path.join(
        get_package_share_directory(package_name),
        'worlds', 'gazebo_models_worlds_collection', 'worlds', 'office_small.world'
    )
    gazebo_models_env = SetEnvironmentVariable(
        name='GAZEBO_MODEL_PATH', 
        value=os.path.join(get_package_share_directory(package_name), 'worlds', 'gazebo_models_worlds_collection', 'models')
    )

    # launch gazebo
    pkg_gazebo_ros = get_package_share_directory('gazebo_ros')

    gzserver_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_gazebo_ros, 'launch', 'gzserver.launch.py')
        ),
        launch_arguments={'world': world}.items()
    )

    gzclient_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_gazebo_ros, 'launch', 'gzclient.launch.py')
        )
    )

    # gazebo = IncludeLaunchDescription(
    #     PythonLaunchDescriptionSource([os.path.join(pkg_gazebo_ros, 'launch', 'gazebo.launch.py')]),
    # )
 
    # Run the spawner node from the gazebo_ros package. The entity name doesn't really matter if you only have a single robot.
    spawn_entity = Node(package='gazebo_ros', executable='spawn_entity.py',
                        arguments=['-topic', 'robot_description',
                                   '-entity', 'my_bot',
                                   '-x', '0.12',
                                   '-y', '2.98',
                                   '-z', '0.1'],
                        output='screen')
 
    # Launch them all!
    return LaunchDescription([
        gazebo_models_env,
        rsp,
        gzserver_cmd,
        gzclient_cmd,
        spawn_entity,
    ])