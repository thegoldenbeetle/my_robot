## Robot Package

When cloning for fetching sumbodules:
`git submodule init`
`git submodule update`

Build and run environment commands:
- `sudo docker-compose -f ./docker-compose.yml build shell`
- `sudo docker-compose -f ./docker-compose.yml run --rm shell`

Build ros package:
- `colcon build --symlink-install`

Launch robot_state_publisher node:
- `ros2 launch my_robot rsp.launch.py`
- `ros2 run joint_state_publisher_gui joint_state_publisher_gui`

Launch robot in gazebo:
- `ros2 launch my_robot launch_sim.launch.py`
- `ros2 run teleop_twist_keyboard teleop_twist_keyboard` - control robot in gazebo

References:
- gazebo tutorial http://classic.gazebosim.org/tutorials

Source:
- `. install/setup.bash` (project)
- `. /opt/ros/foxy/setup.bash` (after install)
- `. /usr/share/gazebo/setup.sh` (in case of gazebo error)

Docs:
- find /opt/ros/foxy/ -name "spawn_entity.py"

