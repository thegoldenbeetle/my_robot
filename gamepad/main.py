from inputs import get_gamepad
import serial
import argparse
from threading import Lock


# Function to map a joystick input value from its range to motor output range
def scale_input(value, in_min, in_max, out_min, out_max):
    return (value - in_min) * (out_max - out_min) // (in_max - in_min) + out_min


# Function to control the motors based on joystick input
def control_motors(x, y):
    # Scale joystick values to motor range (-32000, 32000) -> (-255, 255)
    x_scaled = scale_input(x, -32000, 32000, -255, 255)
    y_scaled = scale_input(y, -32000, 32000, -255, 255)

    # Calculate motor speeds
    left_motor_speed = y_scaled + x_scaled  # Left motor is faster when turning right (x > 0)
    right_motor_speed = y_scaled - x_scaled  # Right motor is faster when turning left (x < 0)

    # Ensure the motor speeds are within the acceptable range (-255 to 255)
    left_motor_speed = max(min(left_motor_speed, 255), -255)
    right_motor_speed = max(min(right_motor_speed, 255), -255)

    return left_motor_speed, right_motor_speed


def loop(conn, mutex):
    current_events = []
    while True:
        events = get_gamepad()
        for event in events:     
            if event.code in ["ABS_X", "ABS_Y"]:
                current_events.append(event) 
            if event.code == "SYN_REPORT":
                process_events(conn, mutex, current_events)
                # print(current_events)
                current_events = []
                # print(event.ev_type, event.code, event.state)


def process_events(conn, mutex, events):
    x_value = 0
    y_value = 0
    for event in events:
        if event.code == "ABS_X":
            x_value = event.state
        if event.code == "ABS_Y":
            y_value = event.state

    left_motor, right_motor = control_motors(x_value, y_value)
    send_command(conn, mutex, f"m {int(left_motor)} {int(right_motor)}")


def send_command(conn, mutex, cmd_string):
    
    mutex.acquire()
    try:
        cmd_string += "\r"
        conn.write(cmd_string.encode("utf-8"))
        if True:
            print("Sent: " + cmd_string)
        ## Adapted from original
        c = ''
        value = ''
        while c != '\r':
            c = conn.read(1).decode("utf-8")
            if (c == ''):
                print("Error: Serial timeout on command: " + cmd_string)
                return ''
            value += c
        value = value.strip('\r')
        if True:
            print("Received: " + value)
        return value
    finally:
        mutex.release()

    
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Serial port configuration")
    parser.add_argument(
        '--port', 
        type=str, 
        default='/dev/ttyUSB0',
        help="The serial port to use (e.g., COM3, /dev/ttyUSB0)"
    )
    parser.add_argument(
        '--baudrate', 
        type=int, 
        default=57600, 
        help="The baud rate to use (e.g., 9600, 115200)"
    )
    args = parser.parse_args()

    conn = serial.Serial(args.serial_port, args.baud_rate, timeout=1.0)

    mutex = Lock()

    loop(conn, mutex)
    conn.close()